.DEFAULT_GOAL := package

.PHONY: uninstall
uninstall: stop
	-sudo pacman -R i3rs --noconfirm

.PHONY: stop
stop:
	-systemctl --user stop i3rs

.PHONY: package
package:
	makepkg -f --noconfirm

.PHONY: install
install:
	makepkg -fi --noconfirm
	systemctl --user daemon-reload
	systemctl --user start i3rs

.PHONY: logs
logs:
	id=$$(systemctl --user show -p InvocationID --value i3rs) \
		&& journalctl --user INVOCATION_ID=$$id + _SYSTEMD_INVOCATION_ID=$$id -fn 100

.PHONY: reinstall
reinstall: uninstall install logs
